package com.yuifei.Main;

import java.io.File;
import java.io.IOException;

import com.android.uiautomator.core.UiDevice;
import com.android.uiautomator.core.UiObject;
import com.android.uiautomator.core.UiObjectNotFoundException;
import com.android.uiautomator.core.UiScrollable;
import com.android.uiautomator.core.UiSelector;
import com.android.uiautomator.testrunner.UiAutomatorTestCase;

/*
 * create by yuyifei
 * 2015-5-26
 */
public class AutoClick extends UiAutomatorTestCase {
	private void clickAliPay() throws UiObjectNotFoundException, InterruptedException, IOException {
		//get uidevice instance
		UiDevice device = getUiDevice();
		
		//click “当面付”
		UiObject payTab = new UiObject(new UiSelector().text("当面付"));
		payTab.clickAndWaitForNewWindow();//click
		
		//click “收”
		UiObject shouTab = new UiObject(new UiSelector().text("收"));
		shouTab.clickAndWaitForNewWindow();//click
		
		//click “设置金额”
		UiObject setMoneyTab = new UiObject(new UiSelector().text("设置金额"));
		setMoneyTab.clickAndWaitForNewWindow();//click
		
		//click “收款金额”
		UiObject moneyTab = new UiObject(new UiSelector().text("金额"));
		moneyTab.clickAndWaitForNewWindow();//click
		moneyTab.setText("1");//set money eg: 1 RMB.
		
		//click “确定”
		UiObject okTab = new UiObject(new UiSelector().text("确定"));
		okTab.clickAndWaitForNewWindow();//click
		
		Thread.sleep(5000);//sleep 5s
		
//		//click “清楚金额”
//		UiObject clearTab = new UiObject(new UiSelector().text("清楚金额"));
//		clearTab.clickAndWaitForNewWindow();//click
		
		//click back one time, turn to "收" interface
		device.pressBack();
		
		//click back one time, turn to aliPay main interface
		device.pressBack();
		
		//click “账单”
		UiObject billTab = new UiObject(new UiSelector().text("账单"));
		billTab.clickAndWaitForNewWindow();//click
		
		//下拉更新列表
	    UiScrollable uiScrollable = new UiScrollable(new UiSelector().scrollable(true));
	    uiScrollable.scrollBackward(10);
	    
/*	    //dump bill content  to dump.xml
	    //need to mkdir /data/local/tmp/local/tmp/ dir and dump.xml file
	    File file = new File("/data/local/tmp/local/tmp/dump.xml");
	    if(!file.exists())
	    	file.createNewFile();
	   
	    String realPath = "dump.xml";
	    device.dumpWindowHierarchy("dump.xml");
	    
	    //TODO
	    //解析dump.xml 获取账单信息
*/		
		//click back one time, quit aliPay
		device.pressBack();
	}
}
