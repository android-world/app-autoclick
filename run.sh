#!/bin/sh

#create uitest protect and a build.xml
#eg. <android-sdk>/tools/android create uitest-project -n <name> -t 1 -p <path>
#<name> ---- the name of project
#<path> ---- the root path of project
# -t <number>--- the number is api version symbol, you can get it through run the command: "android list targets", the ID number is it.
android create uitest-project -n AutoClickAliPay -t 7 -p ../AutoClickAliPay

#set ANDROID_HOME var
#eg. export ANDROID_HOME=<path_to_your_sdk>
export ANDROID_HOME=/home/pro/tools/adt-bundle-linux-x86-20140702/sdk

#run ant use build.xml, and generate xxx.jar
ant build

#push xx.jar to phone device
#adb push bin/test-uiautomator.jar /data/local/tmp

#run uiautomator to test app use xx.jar
#adb shell uiautomator runtest test-uiautomator.jar -c com.yuyifei.test.LaunchSettings

#finish
#echo "run finish..."


